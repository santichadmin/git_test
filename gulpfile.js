var gulp = require("gulp"),
    sass = require("gulp-sass"),
    sourcemaps = require("gulp-sourcemaps"),
    concat = require("gulp-concat"),
    ts = require("gulp-typescript"),
    uglify = require("gulp-uglify"),
    browserSync = require("browser-sync").create()
;

var srcPaths = {
    html: "src/html/",
    scss: "src/scss/",
    ts: "src/ts/",
    ts1: "src/ts/"
}

var distPaths = {
    html: "dist/html",
    scss: "dist/scss/",
    css: "dist/css",
    ts: "dist/ts/",
    js: "dist/js/"
}

gulp.task("html", function (){
    return gulp.src(srcPaths.html+"*.html")
        .pipe(gulp.dest(distPaths.html))
        .pipe(browserSync.stream());
});

gulp.task("scss", function (){
    return gulp.src(srcPaths.scss+"*.scss")
        .pipe(gulp.dest(distPaths.scss))
        .pipe(browserSync.stream());
});

gulp.task("css", function (){
    return gulp.src(srcPaths.scss+"*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distPaths.css))
        .pipe(browserSync.stream());
});

gulp.task("ts", function (){
    return gulp.src(srcPaths.ts+"*.ts")
        .pipe(gulp.dest(distPaths.ts))
        .pipe(browserSync.stream());
});

var tsProject = ts.createProject("tsconfig.json");

gulp.task("ts1", function (){
    return gulp.src(srcPaths.ts+"*.ts")
        .pipe(tsProject())
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distPaths.js))
        .pipe(browserSync.stream());
});

gulp.task("serve", gulp.parallel('html','scss','css','ts', 'ts1'), function(){
    browserSync.init({
        logLevel: "info",
        browser: ["google chrome"],
        proxy: "localhost:80",
        startPath: "/miapp/dist/html/index.htm"
    });

    gulp.watch(srcPaths.html+"*.html", ["html"]),
    gulp.watch(srcPaths.scss+"*.scss", ["css"]),
    gulp.watch(srcPaths.ts+"*.ts", ["ts"]);

});

gulp.task("default", gulp.parallel('serve'), function(){});